
from django.contrib import admin
from django.urls import path, include

from commentsapp.views import CommentListCreateView, CommentAllListView

urlpatterns = [
    path('comments/', CommentListCreateView.as_view()),
    path('comments/all/', CommentAllListView.as_view()),
]
