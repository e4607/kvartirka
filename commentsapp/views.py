from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView, ListAPIView, CreateAPIView
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from commentsapp.models import Article, Comment
from commentsapp.serializers import CommentSerializer, CommentCreateSerializer
from django.contrib.auth.models import User
from rest_framework.exceptions import APIException
import pydantic


class CommentListCreateView(ListAPIView, CreateAPIView):
    """Create and list for comments"""

    serializer_class = CommentSerializer

    def get_queryset(self):

        tail =self.request.GET.get("tail")
        is_true = lambda value: bool(value) and value.lower() not in ('false', '0')

        if is_true(tail) is not True:
            tail = False

        article_id = self.request.GET.get("article_id")
        if tail == True:
            if article_id and article_id.isdigit():
                article = Article.objects.get(id=article_id)
                qs = Comment.objects.filter(response_level__gte=4, article=article)
            else:
                raise APIException(detail="article id required", code=422)
        else:
            if article_id and article_id.isdigit():
                article = Article.objects.get(id=article_id)
                qs = Comment.objects.filter(response_level__lte=3, article=article)
            else:
                qs = Comment.objects.filter(response_level__lte=3)
        return qs

    def get(self, request):
        """Show list of comments with first 3 comments"""
        qs = self.get_queryset()
        serializer = CommentSerializer(qs, many=True)
        return Response(serializer.data)

    def post(self, request):
        comment = CommentCreateSerializer(data=request.data)
        if comment.is_valid():
            comment.save()
            return Response(status=201)
        return Response(status=422)


class CommentAllListView(ListAPIView):
    """Show all comment for article_id or all if article_id is not specified"""

    serializer_class = CommentCreateSerializer

    def get_queryset(self):
        article_id = self.request.GET.get("article_id")
        if article_id and article_id.isdigit():
            article = Article.objects.get(id=article_id)
            qs = Comment.objects.filter(article=article)
        else:
            qs = Comment.objects.all()
        return qs
