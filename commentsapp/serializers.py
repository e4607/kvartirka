from rest_framework.serializers import ModelSerializer, SlugRelatedField

from commentsapp.models import Article, Comment


class CommentSerializer(ModelSerializer):
    author = SlugRelatedField(slug_field="username", read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class CommentCreateSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = ["author", "parent", "response_level", "body", "article"]

