import random

from django.contrib.auth.models import User
from commentsapp.models import Article, Comment
from random_username.generate import generate_username
# from django.db.models import Max

def cleardb():
    Comment.objects.all().delete()
    Article.objects.all().delete()
    User.objects.all().delete()


def create_special_user():
    special_user = User(username="default", password="default", is_active=False, is_staff=False, is_superuser=False)
    special_user.pk = 0;
    special_user.save()


def create_admin_user(username, password):
    admin = User.objects.create_user(username=username, password=password)
    admin.is_superuser = True
    admin.is_staff = True
    admin.is_active = True
    admin.save()


def create_users(count):
    '''Создает пользователей со случаныйными именами в колличестве count'''
    usernames = generate_username(count)
    for name in usernames:
        user = User.objects.create_user(username=name, password=f"{name}-password")
        user.save()


def generate_articles(author, min_article_num=1, max_article_num=3):
    '''
        создает статью для кадого использователей в колличестве от min_article_num
         до count max_article_num
    '''

    for num in range(1, random.randint(min_article_num, max_article_num + 1)):
        article = Article()
        article.name = f"Arclie{num} of {author.username}"
        article.body = f"Body of article{num} of {author.username}"
        article.author = author
        article.save()


def create_articles(users, min_article_num=2, max_article_num=3):
    article_zero = Article()
    article_zero.pk = 0
    article_zero.author = User.objects.get(pk=0)

    for user in users:
        generate_articles(user, min_article_num, max_article_num)


def generate_comments(article, users, min_level=1, max_level=5):
    """
        Для каждой статьи создает коментарии от разных пользователей,
        так чтобы на ответ одного пользователя, был ответ случайного другого,
        где максимальное количество ответов не превышает max_level
    """

    def create_first_comment(article, user):
        comment = Comment()
        comment.article = article
        comment.author = user
        comment.body = f"root comment from user {user.username} for article {article.name}."
        return comment

    def create_child_comment(previous_comment, article, user, comment_level):
        comment = Comment()
        comment.article = article
        comment.author = user
        comment.parent = previous_comment.pk
        comment.response_level = previous_comment.response_level + 1
        comment.body = f"comment level {comment_level+1} from user {user.username}"
        return comment

    """shuffle user list"""
    users = sorted(users, key=lambda x: random.random())
    cur_user_idx = 0
    max_users = len(users)
    previous_comment_level = -1

    while previous_comment_level < random.choice([i for i in range(0, max_level)]):
        if previous_comment_level == -1:
            user = users[cur_user_idx]
            comment = create_first_comment(article, user)
            comment.save()
            cur_user_idx += 1
            previous_comment_level = 0
        else:
            # previous_comment_level = Comment.objects.filter(article=article.pk). \
            #     aggregate(Max('response_level'))['response_level__max']

            user = users[cur_user_idx]
            previous_comment = Comment.objects.get(article_id=article.pk, response_level=previous_comment_level)
            comment = create_child_comment(previous_comment, article, user, previous_comment_level)
            comment.save()
            cur_user_idx += 1
            previous_comment_level += 1
            if cur_user_idx == max_users:
                cur_user_idx = 0
