from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from commentsapp.management.common import cleardb, create_admin_user, create_users, create_articles, \
    create_special_user, generate_comments
from commentsapp.models import Article, Comment


class Command(BaseCommand):

    def handle(self, *args, **options):
        """Fill DB with testdata """

        create_admin_user(username="admin", password="admin")
        create_special_user()
        create_users(5)
        users = User.objects.all().exclude(username="admin").exclude(pk=0)
        if len(users) < 2:
            print("Create more then 1 user!!!")
            exit(1)
        create_articles(users, min_article_num=1, max_article_num=2)

        articles = Article.objects.all()

        for article in articles:
            generate_comments(article=article, users=users, min_level=5, max_level=10)
        pass
