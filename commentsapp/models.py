from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Article(models.Model):
    name = models.CharField(max_length=128)
    body = models.TextField(blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    create_timestamp = models.DateTimeField(auto_now_add=True)
    update_timestamp = models.DateTimeField(auto_now=True)

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.PROTECT)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    parent = models.PositiveIntegerField(default=0)
    response_level = models.PositiveIntegerField(default=0)
    body = models.TextField(blank=False, null=False)
    is_deleted = models.BooleanField(default=False)
    create_timestamp = models.DateTimeField(auto_now_add=True)
    update_timestamp = models.DateTimeField(auto_now=True)
