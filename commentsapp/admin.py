from django.contrib import admin

# Register your models here.
from django.contrib.admin import display

from commentsapp.models import Article, Comment

# admin.site.register(Article)
# admin.site.register(Comment)

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'create_timestamp', 'update_timestamp',)
    fields = ('name', 'body', 'author', 'create_timestamp', 'update_timestamp',)
    readonly_fields = ('create_timestamp','update_timestamp')
    ordering = ('author', 'name',)
    search_fields = ('name',)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('get_article_name', 'get_author_name', 'response_level', 'is_deleted',)
    exclude = ('create_timestamp',)
    readonly_fields = ('update_timestamp', 'response_level', 'article')
    ordering = ('article', 'response_level',)


    @display(description='article name')
    def get_article_name(self, obj):
        return obj.article.name

    @display(ordering='article__name', description='author name')
    def get_author_name(self, obj):
        return obj.author.username