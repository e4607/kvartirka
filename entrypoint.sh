#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

if [ ! -f ./secret.env ]; then
    secret=`python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())'`
    echo "SECRET_KEY='${secret}'" >  secret.env
    python manage.py migrate
    python manage.py filldb
fi


exec "$@"

