# kvartirka

'comments api' test project

## Выполнено

Добавление статьи (Можно чисто номинально, как сущность, к которой крепятся комментарии).
Добавление комментария к статье.
Добавление коментария в ответ на другой комментарий (возможна любая вложенность).
Получение всех комментариев к статье вплоть до 3 уровня вложенности.
Получение всех вложенных комментариев для комментария 3 уровня.
По ответу API комментариев можно воссоздать древовидную структуру.

Не успел:
Опубликовать Docker Image 
Настроить CI/CD
Написать тесты

# Инструкция по запуску
! В процессе запуска генерируются тестовые данные, может занять некоторое время

```
git clone git@gitlab.com:e4607/kvartirka.git
cd kvartirka
docker-compose up
```

# Описание API

/api/v1/comments/ принимает GET и POST запросы
```
# вернет все комментарии ко всем статьям, где ответов на первый комментарий не больше 3
curl -X GET "http://localhost:8000/api/v1/comments/" 

# вернет комментарии к статье article_id=2, где ответов на первый комментарий не больше 3
curl -X GET "http://localhost:8000/api/v1/comments/?article_id=2" 

# вернет комментарии к статье article_id=2, где уровень ответа на первый комментарий больше 3
curl -X GET "http://localhost:8000/api/v1/comments/?article_id=2&tail=1" 

# Добавление комментария
curl -X POST "http://localhost:8000/api/v1/comments/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"parent\": 0,  \"response_level\": 0,  \"body\": \"test\",  \"is_deleted\": false,  \"article\": 2,  \"author\" : 2}"
```

Для добавления в json обязательна передачи следующих полей
```
{
  "parent": 0,
  "response_level": 0,
  "body": "test",
  "is_deleted": false,
  "article": 2,
  "author" : 2
}
```

"parent" - ID родительского комментария
"response_level" - Уровень вложенности ответа. вычисляется на стороне клиента (по задумке нужно вычислять в ORM, но не успел сделать)
"body" - текст комментария
"is_deleted" - bool поле для скрытия комментария
"article": id статьи
"author" - id пользователя

Для удобства проверки добавлен
```
# вернет все запросы без ограничения вложенности
curl -X GET "http://localhost:8000/api/v1/comments/all/"
```
# Примеры запросов

curl -X GET "http://localhost:8000/api/v1/comments/?article_id=2&tail=1"

# swagger
Swagger доступен по адресу
```
http://localhost:8000/swagger/
```
